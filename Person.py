"""Creates sender, receiver and a hacker"""
from Cipher import Cipher, Caesar, MultiplicationCipher, UnbreakableCipher, AffineCipher, RSA


class Person:
    """Creates a person object which have a key"""

    def __init__(self):
        cipher = Cipher()
        self.key = cipher.key

    def set_key(self, key):
        """Sets the key for the person"""
        self.key = key

    def get_key(self):
        """Gets the persons key"""
        return self.key


class Sender(Person):

    def __init__(self):
        super().__init__()

    def send(self, cipher, text):
        """Send an encrypted message"""
        message = cipher.encode(text)
        return message


class Receiver(Person):

    def __init__(self):
        super().__init__()

    def receive(self, cipher, message):
        """decodes a encoded message from the sender"""
        text = cipher.decode(message)
        return text


class HackerMan:

    def hack(self, cipher, encrypted_text):
        """Tries to decode a encoded message, tries 100 different keys
        :param cipher: cipher you want to use when u decode the message
        :param encrypted_text: encoded message from the sender
        :return message:
        """
        for i in range(100):
            cipher.set_key(i)
            text = cipher.decode(encrypted_text)
            text = str.split(text, " ")
            print(text[0])
            if self.compare_words(text[0]):
                message = ""
                for word in text:
                    message += word + " "
                return message

    def hack_unbreakable(self, encrypted_text):
        """Hacks the unbreakable cipher with the english_words.txt as key
        :param encrypted_text: encoded text from the sender
        :return message: Decoded message
        """
        file = open("english_words.txt", "r")
        for line in file:
            unbreakable = UnbreakableCipher(str.rstrip(line))
            text = unbreakable.decode(encrypted_text)
            text = str.split(text, " ")
            if self.compare_words(text[0]):
                message = ""
                for word in text:
                    message += word + " "
                return message

    def compare_words(self, word):
        """Compares a word with a english word from a file
        :param word: Word to compare
        :return boolean: Returns True if word is a english word
        """
        file = open("english_words.txt", "r")
        for line in file:
            if str.rstrip(line) == word:
                return True
        return False


class Test():
    """Testmethod so we can see if every Cipher works"""
    sender = Sender()
    receiver = Receiver()
    hacker = HackerMan()
    caesar = Caesar()
    multi = MultiplicationCipher()
    affine = AffineCipher()
    unbreakable = UnbreakableCipher("aahs")
    rsa = RSA()

    caesar_message = sender.send(caesar, "this is a test")
    multi_message = sender.send(multi, "this is a test")
    affine_message = sender.send(affine, "this is a test")
    unbreakable_message = sender.send(unbreakable, "this is a test")
    rsa_message = sender.send(rsa, "this is a test")

    print("krypterte meldinger")
    print(caesar_message)
    print(multi_message)
    print(affine_message)
    print(unbreakable_message)
    print(rsa_message)
    print()
    print("Caesar: " + receiver.receive(caesar, caesar_message))
    print("Multi: " + receiver.receive(multi, multi_message))
    print("Affine: " + receiver.receive(affine, affine_message))
    print("Unbreakable: " + receiver.receive(unbreakable, unbreakable_message))
    print("RSA: " + receiver.receive(rsa, rsa_message))

    print("Hack caesar: " + hacker.hack(caesar, caesar_message))
    print("Hack multi: " + hacker.hack(multi, multi_message))
    print("Hack affine: " + hacker.hack(affine, affine_message))
    print("Hack unbreakable: " + hacker.hack_unbreakable(unbreakable_message))


def main():
    Test()


if __name__ == '__main__':
    main()
