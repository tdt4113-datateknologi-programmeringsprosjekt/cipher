"""A variety of Ciphers to encode and decode messages"""
import random
import crypto_utils as cu


class Cipher:
    """SuperClass used to assign an alphabet and a key"""
    def __init__(self):
        self.alphabet = []
        for i in range(32, 127):
            self.alphabet.append(chr(i))
        self.key = self.generate_key()

    def generate_key(self):
        """Generates a random key
        :return: random key
        """
        return random.randint(1, 95)

    def verify(self, text):
        """method to test encode and decode for the subclasses
        :param text: Text to encode and decode
        :return encrypted message, message sent
        """
        message = self.encode(text)
        r_message = self.decode(message)
        return message, r_message

    def encode(self, text):
        """Dummy method"""
        return "no selected Cipher"

    def decode(self, message):
        """Dummy method"""
        return "nothing to decode"

    def set_key(self, number):
        """Sets the key"""
        self.key = number


class Caesar (Cipher):
    """Caesar Cipher Method, shift key to the side"""
    def __init__(self):
        super().__init__()

    def encode(self, text):
        message = ""
        alphabet = self.alphabet
        for letter in text:
            for i in range(len(alphabet)):
                if letter == alphabet[i]:
                    message += alphabet[(i + self.key)%95]
        return message

    def decode(self, message):
        alphabet = self.alphabet
        decoded_message = ""
        for letter in message:
            for i in range(len(alphabet)):
                if letter == alphabet[i]:
                    decoded_message += alphabet[(i - self.key)%95]
        return decoded_message


class MultiplicationCipher(Cipher):
    """Multiplies value and gives each letter another value"""
    def __init__(self):
        super().__init__()
        self.inverse = cu.modular_inverse(self.key, len(self.alphabet))
        while self.inverse * self.key % len(self.alphabet) != 1:
            self.key = self.generate_key()
            self.inverse = cu.modular_inverse(self.key, len(self.alphabet))

    def encode(self, text):
        message = ""
        alphabet = self.alphabet
        for letter in text:
            for i in range(len(alphabet)):
                if letter == alphabet[i]:
                    message += alphabet[(self.key*i)%95]
        return message

    def decode(self, message):
        alphabet = self.alphabet
        decoded_message = ""
        for letter in message:
            for i in range(len(alphabet)):
                if letter == alphabet[i]:
                    decoded_message += alphabet[(self.inverse*(i))%95]
        return decoded_message


class AffineCipher(Cipher):

    def __init__(self):
        super().__init__()
        self.key2 = self.generate_key()
        self.inverse_key()
        self.cipher1 = Caesar()
        self.cipher2 = MultiplicationCipher()

    # Switching keys after encode

    def encode(self, text):
        message = self.cipher2.encode(text)
        self.key_switch(self.cipher1, self.cipher2)
        text = self.cipher1.encode(message)
        return text

    def decode(self, message):
        text = self.cipher1.decode(message)
        self.key_switch(self.cipher1, self.cipher2)
        text = self.cipher2.decode(text)
        return text

    def key_switch(self, cipher1, cipher2):
        """Switches the keys in the 2 cyphers
        :param cipher1, cipher2: Ciphers to switch keys
        """
        k1 = cipher1.key
        k2 = cipher2.key
        cipher1.set_key(k2)
        cipher2.set_key(k1)

    def verify(self, text):
        message = self.encode(text)
        decoded_message = self.decode(message)
        return message, decoded_message

    def inverse_key(self):
        """Creates a key for Caesar such that multi can use it as inverse"""
        inverse = cu.modular_inverse(self.key, len(self.alphabet))
        while inverse * self.key % len(self.alphabet) != 1:
            self.key = self.generate_key()
            inverse = cu.modular_inverse(self.key, len(self.alphabet))


class UnbreakableCipher(Cipher):
    """Encodes and decodes with a keyword instead of number"""
    def __init__(self, keyword):
        super().__init__()
        self.key = keyword

    def encode(self, text):
        message = ""
        alphabet = self.alphabet
        i = 0
        for letter in text:
            message += alphabet[(((ord(letter) - 32) + (ord(self.key[i]) - 32)) % 95)]
            i += 1
            if i == len(self.key):
                i = 0
        return message

    def decode(self, message):
        decrytion_key = ""
        text = ""
        alphabet = self.alphabet
        len_alp = len(alphabet)
        x = 0
        for i in range(len(self.key)):
            keyword_position = (ord(self.key[i]) - 32)
            decrytion_key += alphabet[(len_alp - keyword_position) % 95]
        for letter in message:
            letter_value = (ord(letter) - 32)
            decrypted_key_value = (ord(decrytion_key[x]) - 32)
            text += alphabet[(letter_value + decrypted_key_value) % 95]
            x += 1
            if x == len(decrytion_key):
                x = 0
        return text


class RSA(Cipher):
    """Encodes and decodes with prime numbers.
    Uses key pairs for encryption and a different pair for decoding
    """
    def __init__(self):
        super().__init__()
        self._p, self._q = self.generate_prime()
        self._n = self._p * self._q
        self._o = (self._p - 1)*(self._q - 1)
        self._e = random.randint(3, self._o-1)
        self._d = cu.modular_inverse(self._e, self._o)
        while self._d * self._e % self._o != 1:
            self._e = random.randint(3, self._o-1)
            self._d = cu.modular_inverse(self._e, self._o)
        self._t = random.randint(0, self._n)

    def generate_prime(self):
        """Generates 2 primes with 8 bits"""
        _p = cu.generate_random_prime(8)
        _q = cu.generate_random_prime(8)
        while _p == _q:
            _p = cu.generate_random_prime(8)
        return _p, _q

    def encryption_of_integer_message(self, letter):
        """Encrypts a single letter
        :param letter: Letter to encrypt
        """
        _c = pow(letter, self._e, self._n)
        return _c

    def decryption_of_integer_message(self, _c):
        """Decodes the encoded message"""
        message = pow(_c, self._d, self._n)
        return message

    def encode(self, text):
        """Uses blocks from text and decryption of integer to encode the message"""
        encoded = []
        block = cu.blocks_from_text(text, 2)
        for number in block:
            encoded.append(self.encryption_of_integer_message(number))
        return encoded

    def decode(self, message):
        """Uses text from blocks and decoding of integer to decode the message"""
        m = []
        for b in message:
            m.append(self.decryption_of_integer_message(b))
        text = cu.text_from_blocks(m, 8)
        return text
